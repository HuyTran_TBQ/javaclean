package vn.kms.launch.cleancode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ConstantValue {
    public final Set<String> m_valid_state_codes = new HashSet<>(Arrays.asList("AL", "AK", "AS", "AZ", "AR", "CA", "CO",
            "CT", "DE", "DC", "FL", "GA", "GU", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MH", "MA",
            "MI", "FM", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "MP", "OH", "OK", "OR",
            "PW", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "VI", "WA", "WV", "WI", "WY"));
    public final Map<String, String> report_headers = new HashMap<String, String>() {
        {
            put("invalid-contact-details", "contact_id\terror_field\terror_message\r\n");
            put("invalid-contact-summary", "field_name\tnumber_of_invalid_contact\r\n");
            put("contact-per-state", "state_code\tnumber_of_contact\r\n");
            put("contact-per-age-group", "group\tnumber_of_contact\tpercentage_of_contact\r\n");
        }
    };
    public final int year_of_Report = 2017;
}
