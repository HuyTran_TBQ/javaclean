package vn.kms.launch.cleancode;

import java.util.*;

import Contact.Contact;
import Contact.ValidationContact;
import LoadFile.LoadFile;
import LoadFile.LoadTSVFile;

public class Application {

    public static void main(String[] args) throws Exception {
        Map<String, Integer> field_error_counts = new TreeMap<>(); // I want to sort by key
        Map<Integer, Map<String, String>> invalidContacts = new LinkedHashMap<>(); // invalidContacts order by ID
        Map<String, Map<String,Integer>> reports;
        
        LoadFile Load = new LoadTSVFile();
        String[] lines = Load.loadFileString("data/contacts.tsv"); // load text from file to list String
        
        
        LoadContact loadcontact = new LoadContact();
        List<Contact> allContacts = loadcontact.LoadInfo(lines); // store info to list variable
        
        ValidationContact Val = new ValidationContact();
        invalidContacts = Val.ValidateInfo(field_error_counts, allContacts); // check info and save id invalid

        loadcontact.Sort(invalidContacts, allContacts, "First_name");//Sort by First_name|LsName|Address|City|STATE|Z_Code|Mobile_Phone|Email
        
        StoreData Store = new StoreData();
        Store.StoreValidContact(invalidContacts, allContacts); //store valid contacts to file
        
        reports = loadcontact.NumContactPerState(invalidContacts, allContacts);
        Store.StoreContactperState(reports);
        reports = loadcontact.NumContactPerAge(invalidContacts, allContacts);
        Store.StoreContactPerAgeGroup(reports);
        Store.StoreInvalidContactSummary(field_error_counts);
        Store.StoreInvalidDetail(invalidContacts);
        
    }

    // TODO: Calculate age exactly by month/day/year.
    public static int precise_calculate_age(String date_of_birth) {
        return 10;
    }

    
}
