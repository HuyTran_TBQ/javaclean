package vn.kms.launch.cleancode;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Contact.Contact;
import LoadFile.LoadFile;
import LoadFile.LoadTSVFile;

public class LoadContact {
    private Map<String, Integer> header;
    private String[] data;
    public List<Contact> LoadInfo(String[] lines) {
        List<Contact> allContacts = new ArrayList<>();
        LoadFile loadfile = new LoadTSVFile();
        header = loadfile.LoadHeader(lines[0]);  //Map name of column with its ID
        
        for (int i = 1; i < lines.length; i++) {
            if(!checkinfo(lines[i])){
                continue;
            }
            
            allContacts.add(setInfo()); // Add contact to list
        }
        return allContacts;
    }
    private boolean checkinfo(String line){
        if (line.trim().length() == 0) {
            return false;
        }
        
        data = line.split("\t"); // get data of a line

        if (data.length != 14) { // invalid line format
            return false;
        }
        try {
            Integer.parseInt(data[header.get("id")]);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
    private Contact setInfo(){
        // TODO: I will use reflection & annotations to build contact object later
        Contact contact = new Contact();
        contact.setId(Integer.parseInt(data[header.get("id")]));
        contact.setFirst_name(data[header.get("first_name")]);
        contact.setLsName(data[header.get("last_name")]);    
        contact.day_of_birth = data[header.get("date_of_birth")];
        contact.setAddress(data[header.get("address")]); //
        contact.setCity(data[header.get("city")]);
        contact.setSTATE(data[header.get("state")]);
        contact.setZ_Code(data[header.get("zip")]); 
        contact.setMobile_Phone(data[header.get("phone1")]);
        contact.setEmail(data[header.get("email")]);
        return contact;
    }
    
    public void Sort(Map<Integer, Map<String, String>> invalidContacts, List<Contact> allContacts,String SortColumnName)
            throws Exception {
        // Sort contact by Name of Column: SortColumnName
        Class<Contact> CONTACT = Contact.class;
        Method setMethod = CONTACT.getMethod("get"+SortColumnName);
        for (int i = 0; i < allContacts.size(); i++) {
            for (int j = allContacts.size() - 1; j > i; j--) {
                Contact contact_a = allContacts.get(i);
                Contact contact_b = allContacts.get(j);
                
                if (!invalidContacts.containsKey(contact_a.getId())
                        && !invalidContacts.containsKey(contact_b.getId())) {
                    String A = (String)setMethod.invoke(contact_a);
                    String B = (String)setMethod.invoke(contact_b);
                    if (A.compareTo(B)>0) {
                        allContacts.set(i, contact_b);
                        allContacts.set(j, contact_a);
                    }
                }
            }
        }
    }

    public Map<String, Map<String,Integer>> NumContactPerState(Map<Integer, Map<String, String>> invalidContacts, List<Contact> allContacts)
            throws Exception {
        // 5. Generate contact per state report
        Map<String, Map<String,Integer>> reports = new HashMap<>();
        Map<String, Integer> r_contact_per_state = new HashMap<>();
        for (Contact contact : allContacts) {
            if (!invalidContacts.containsKey(contact.getId())) {
                int state_count = 0;
                if (r_contact_per_state.containsKey(contact.getSTATE())) {
                    state_count = r_contact_per_state.get(contact.getSTATE());
                }
                r_contact_per_state.put(contact.getSTATE(), state_count + 1);
            }
        }

        reports.put("contact-per-state", r_contact_per_state);
        return reports;
    }
    public Map<String, Map<String,Integer>> NumContactPerAge(Map<Integer, Map<String, String>> invalidContacts, List<Contact> allContacts)
            throws Exception {
        // Generate contact per age report
        Map<String, Map<String,Integer>> reports = new HashMap<>();
        Map<String, Integer> r_contact_per_age_group = new HashMap<>();
        int age_group_count = 0;
        for (Contact contact : allContacts) {
            if (!invalidContacts.containsKey(contact.getId())) {
                age_group_count++;
                
                r_contact_per_age_group.put(calculate_age_group(contact.getAge()), age_group_count + 1);
            }
        }

        reports.put("contact-per-age-group", r_contact_per_age_group);
        return reports;
    }

    private static String calculate_age_group(int age) {
        if (age <= 9) {
            return "Children";
        } else if (age <= 19) {
            return "Adolescent";
        } else if (age <= 45) {
            return "Adult";
        } else if (age <= 60) {
            return "Middle Age";
        } else {
            return "Senior";
        }
    }
}
