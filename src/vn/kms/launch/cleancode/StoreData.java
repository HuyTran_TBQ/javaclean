package vn.kms.launch.cleancode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import Contact.Contact;

public class StoreData {
    private File CreateFolder(String folder) {
        File outputFile = new File(folder);
        if (!outputFile.exists()) {
            outputFile.mkdirs();
        }
        return outputFile;
    }

    public void StoreValidContact(Map<Integer, Map<String, String>> invalidContacts, List<Contact> allContacts)
            throws Exception {
        File outputFile = CreateFolder("output");
        // Store valid data

        Writer writer1 = new FileWriter(new File(outputFile, "valid-contacts.tab"));
        // write header
        writer1.write(
                "id\tfirst_name\tlast_name\tday_of_birth\taddress\tcity\tstate\tzip_code\tmobile_phone\temail\r\n");
        for (Contact contact : allContacts) {
            if (!invalidContacts.containsKey(contact.getId())) {
                writer1.write(contact.toLine() + "\r\n");
            }
        }
        writer1.flush();
        writer1.close();
    }

    public void StoreContactPerAgeGroup(Map reports) throws IOException {
        ConstantValue cons = new ConstantValue();
        String reportName = "contact-per-age-group";
        File outputFile = CreateFolder("output");

        Writer writer = new FileWriter(new File(outputFile, reportName + ".tab"));

        // write header
        writer.write(cons.report_headers.get(reportName));

        Map<String, Integer> report = (Map<String, Integer>) reports.get(reportName);
        int total = 0;
        for (Integer v : report.values()) {
            total += v;
        }
        // sort by age-group followed the requirement
        String[] age_groups = { "Children", "Adolescent", "Adult", "Middle Age", "Senior" };
        for (String item : age_groups) {
            writer.write(item + "\t" + report.get(item) + "\t"
                    + Math.round((report.get(item) * 1000.0f) / total) / 10.0f + "%\r\n");
        }
        writer.close();
        System.out.println("Generated report " + "output/" + reportName + ".tab");
    }

    public void StoreContactperState(Map reports) throws IOException {
        ConstantValue cons = new ConstantValue();

        String reportName = "contact-per-state";
        File outputFile = CreateFolder("output");
        Writer writer = new FileWriter(new File(outputFile, reportName + ".tab"));

        // write header
        writer.write(cons.report_headers.get(reportName));

        Map<String, Integer> report = (Map<String, Integer>) reports.get(reportName);
        report = new TreeMap<>(report); // I want to sort by state
        for (String item : report.keySet()) {
            writer.write(item + "\t" + report.get(item) + "\r\n");
        }
        writer.flush();
        System.out.println("Generated report " + "output/" + reportName + ".tab");
        writer.close();
    }

    public void StoreInvalidContactSummary(Map<String, Integer> field_error_counts) throws IOException {
        ConstantValue cons = new ConstantValue();
        String reportName = "invalid-contact-summary";
        File outputFile = CreateFolder("output");

        Writer writer = new FileWriter(new File(outputFile, reportName + ".tab"));

        // write header
        writer.write(cons.report_headers.get(reportName));

        for (Map.Entry entry : field_error_counts.entrySet()) {
            writer.write(entry.getKey() + "\t" + entry.getValue() + "\r\n");
        }
        writer.flush();
        System.out.println("Generated report " + "output/" + reportName + ".tab");
        writer.close();
    }

    public void StoreInvalidDetail(Map<Integer, Map<String, String>> invalidContacts) throws IOException {
        ConstantValue cons = new ConstantValue();
        String reportName = "invalid-contact-details";
        File outputFile = CreateFolder("output");

        Writer writer = new FileWriter(new File(outputFile, reportName + ".tab"));

        // write header
        writer.write(cons.report_headers.get(reportName));

        for (Map.Entry<Integer, Map<String, String>> entry : invalidContacts.entrySet()) {
            int contactID = entry.getKey();
            Map<String, String> error_by_fields = entry.getValue();
            for (String field_name : error_by_fields.keySet()) {
                writer.write(contactID + "\t" + field_name + "\t" + error_by_fields.get(field_name) + "\r\n");
            }
        }
        writer.close();
        System.out.println("Generated report " + "output/" + reportName + ".tab");
    }
}
