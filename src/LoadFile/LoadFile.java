package LoadFile;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

public interface LoadFile {
    public String[] loadFileString(String path) throws Exception;
    public Map<String, Integer> LoadHeader(String HeaderLine);
}

