package LoadFile;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class LoadTSVFile implements LoadFile {
    @Override
    public String[] loadFileString(String path) throws Exception {
        // Load data from file
        // Store file to String
        InputStream is = new FileInputStream(path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String s = "";
        String l;
        while ((l = reader.readLine()) != null) {
            s += l;
            s +="\r";
        }

        String[] lines = s.split("\r"); // get all lines
        reader.close();
        return lines;
    }
    public Map<String, Integer> LoadHeader(String HeaderLine) {
        String[] header = HeaderLine.split("\t");
        Map<String, Integer> MapHeader = new HashMap<>();
        for (int i = 0; i < header.length; i++) {
            MapHeader.put(header[i], i);
        }
        return MapHeader;
    }
}
