package Contact;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import vn.kms.launch.cleancode.ConstantValue;


public class ValidationContact {
    public Map<Integer, Map<String, String>> ValidateInfo(Map<String, Integer> field_error_counts,
            List<Contact> allContacts) throws Exception {
        //Validate contact data
        // TODO: I will use reflection & annotations to isValid contact data later
        Map<Integer, Map<String, String>> invalidContacts = new TreeMap<>();
        for (Contact contact : allContacts) {
            Map<String, String> errors = new TreeMap<>(); // errors order by field name
            ValidateFirstName(contact, errors, field_error_counts);
            ValidateLastName(contact, errors, field_error_counts);
            ValidateDoB(contact, errors, field_error_counts);
            ValidateAdress(contact, errors, field_error_counts);
            ValidateState_Codes(contact, errors, field_error_counts);
            
            ValidateZ_Code(contact, errors, field_error_counts);
            ValidateMobilePhone(contact, errors, field_error_counts);
            ValidateEmail(contact, errors, field_error_counts);
            
            if (!errors.isEmpty()) {
                invalidContacts.put(contact.getId(), errors);
            } else { 
                contact.setAge(calculate_age_by_year(contact.day_of_birth));
            }
        }
        return invalidContacts;
    }

    private void ValidateFirstName(Contact contact, Map<String, String> errors,
            Map<String, Integer> field_error_counts) {
        if (contact.getFirst_name().trim().length() == 0) {
            errors.put("firstName", "is empty");
            add_FieldERROR(field_error_counts, "first_name");
        }
        if (contact.getFirst_name().length() > 10) {
            errors.put("firstName", "'" + contact.getFirst_name() + "''s length is over 10");
            add_FieldERROR(field_error_counts, "first_name");
        }
    }

    private void ValidateLastName(Contact contact, Map<String, String> errors,
            Map<String, Integer> field_error_counts) {
        if (contact.getLsName().trim().length() == 0) {
            errors.put("lastName", "is empty");
            add_FieldERROR(field_error_counts, "last_name");
        }
        if (contact.getLsName().length() > 10) {
            errors.put("lastName", "'" + contact.getLsName() + "''s length is over 10");
            add_FieldERROR(field_error_counts, "last_name");
        }
    }

    private void add_FieldERROR(Map<String, Integer> counts, String field_name) {
        Integer count = counts.get(field_name);
        if (count == null) {
            count = new Integer(0);
        }
        count = count + 1;
        counts.put(field_name, count);
    }

    private void ValidateDoB(Contact contact, Map<String, String> errors, Map<String, Integer> field_error_counts) {
        if (contact.day_of_birth == null || contact.day_of_birth.trim().length() != 10) {
            errors.put("day_of_birth", "'" + contact.day_of_birth + "' is invalid");
            add_FieldERROR(field_error_counts, "day_of_birth");
        }
    }

    private void ValidateAdress(Contact contact, Map<String, String> errors, Map<String, Integer> field_error_counts) {
        if (contact.getAddress().length() > 20) {
            errors.put("address", "'" + contact.getAddress() + "''s length is over 20");
            add_FieldERROR(field_error_counts, "address");
        }
        if (contact.getCity().length() > 15) {
            errors.put("city", "'" + contact.getCity() + "''s length is over 15");
            add_FieldERROR(field_error_counts, "city");
        }
    }
    
    private void ValidateZ_Code(Contact contact, Map<String, String> errors, Map<String, Integer> field_error_counts) {
        if (!contact.getZ_Code().matches("^\\d{4,5}$")) {
            errors.put("zipCode", "'" + contact.getZ_Code() + "' is not four or five digits");
            add_FieldERROR(field_error_counts, "zip_code");
        }
    }
    private void ValidateMobilePhone(Contact contact, Map<String, String> errors, Map<String, Integer> field_error_counts) {
        if (!contact.getMobile_Phone().matches("^\\d{3}\\-\\d{3}\\-\\d{4}$")) {
            errors.put("mobilePhone", "'" + contact.getMobile_Phone() + "' is invalid format XXX-XXX-XXXX");
            add_FieldERROR(field_error_counts, "mobile_phone");
        }
    }
    private void ValidateEmail(Contact contact, Map<String, String> errors, Map<String, Integer> field_error_counts) {
        if (!contact.getEmail().matches("^.+@.+\\..+$")) {
            errors.put("email", "'" + contact.getEmail() + "' is invalid email format");
            add_FieldERROR(field_error_counts, "email");
        }

    }
    private void ValidateState_Codes(Contact contact, Map<String, String> errors, Map<String, Integer> field_error_counts) {
        ConstantValue cons = new ConstantValue();
        if (!cons.m_valid_state_codes.contains(contact.getSTATE())) { errors.put("state","'" + contact.getSTATE() + "' is incorrect state code");
          add_FieldERROR(field_error_counts, "state"); }
    }

    public int calculate_age_by_year(String date_of_birth) throws NoSuchFieldException, SecurityException {
        ConstantValue cons = new ConstantValue();
        
        String yearStr = date_of_birth.split("/")[2];
        int year = Integer.parseInt(yearStr);
        return  cons.year_of_Report - year;
    }

}
